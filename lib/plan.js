// plan.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const {DatabankObject} = require('databank')
const uuid = require('uuid')
const debug = require('debug')('plan:plan')

const Plan = DatabankObject.subClass('plan')

if (process.env.STRIPE_KEY) {
  debug(`Setting stripe instance to ${process.env.STRIPE_KEY}`)
  Plan.stripe = require('stripe')(process.env.STRIPE_KEY)
}

Plan.schema = {
  pkey: 'id',
  fields: [
    'name',
    'stripeId',
    'apiLimit',
    'period',
    'price',
    'currency',
    'createdAt',
    'updatedAt'
  ],
  indices: ['name', 'stripeId']
}

Plan.beforeCreate = function (props, callback) {
  const required = ['name', 'apiLimit', 'period', 'currency']
  for (const prop of Array.from(required)) {
    if (!props[prop]) {
      callback(new Error(`${prop} property required`))
    }
  }
  props.id = uuid.v1()
  props.createdAt = (props.updatedAt = (new Date()).toISOString())

  if (this.stripe) {
    const { stripe } = this
    debug(`Retrieving plan ${props['stripeId']} from Stripe`)
    stripe.plans.retrieve(props['stripeId'], (err, plan) => {
      if (err) {
        debug(`Error retrieving plan ${props['stripeId']} from Stripe: ${err.message}`)
        debug('Creating plan')
        stripe.plans.create({
          name: props['name'],
          amount: props['price'],
          currency: props['currency'],
          interval: props['period'],
          id: props['stripeId']
        }
          , (err, plan) => {
          if (err) {
            debug(`Error creating plan ${props['stripeId']} on Stripe: ${err.message}`)
            debug(`Error detail: ${JSON.stringify(err.detail)}`)
            callback(err)
          } else {
            callback(null, props)
          }
        })
      } else {
        callback(null, props)
      }
    })
  } else {
    callback(null, props)
  }
}

Plan.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = (new Date()).toISOString()
  callback(null, props)
}

Plan.prototype.beforeSave = function (callback) {
  const props = this
  const required = ['name', 'apiLimit', 'period', 'currency']
  for (const prop of Array.from(required)) {
    if (!props[prop]) {
      callback(new Error(`${prop} property required`))
    }
  }
  props.updatedAt = (new Date()).toISOString()
  if (!props.id) {
    props.id = uuid.v1()
  }
  if (!props.createdAt) {
    props.createdAt = props.updatedAt
  }
  callback(null)
}

Plan.prototype.beforeDel = function (callback) {
  const props = this
  if (this.stripe) {
    this.stripe.plans.del(props.stripeId, (err, confirmation) => {
      if (err) {
        callback(err)
      } else {
        callback(null)
      }
    })
  } else {
    callback(null)
  }
}

Plan.byStripeId = (stripeId, callback) =>
  Plan.search({stripeId}, (err, results) => {
    if (err) {
      callback(err)
    } else if (results.length > 1) {
      callback(new Error(`Too many plans with ID ${stripeId}`))
    } else if (results.length === 0) {
      callback(null, null)
    } else {
      callback(null, results[0])
    }
  })

module.exports = Plan
