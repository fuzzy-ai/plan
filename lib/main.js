// main.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const PlanServer = require('./planserver')

const server = new PlanServer(process.env)

server.start((err) => {
  if (err) {
    return console.error(err)
  } else {
    return console.log('Server started.')
  }
})

// Try to send a slack message on error

process.on('uncaughtException', (err) => {
  if ((server != null) && (server.slackMessage != null)) {
    const msg = `UNCAUGHT EXCEPTION: ${err.message}`
    return server.slackMessage('error', msg, ':bomb:', (err) => {
      if (err != null) {
        return console.error(err)
      }
    })
  }
})

process.on('SIGTERM', () => {
  console.log('Shutting down...')
  return server.stop((err) => {
    if (err) {
      console.error(err)
      return process.exit(-1)
    } else {
      console.log('Done.')
      return process.exit(0)
    }
  })
})
