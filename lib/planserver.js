// planserver.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const Microservice = require('@fuzzy-ai/microservice')
const async = require('async')
const debug = require('debug')('plan:planserver')

const version = require('./version')
const HTTPError = require('./httperror')
const Plan = require('./plan')

class PlanServer extends Microservice {
  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)

    if ((env.STRIPE_KEY == null)) {
      throw new Error('STRIPE_KEY environment variable required')
    }

    cfg.stripeKey = env.STRIPE_KEY
    cfg.stripeHost = env.STRIPE_HOST
    cfg.stripePort = this.envInt(env, 'STRIPE_PORT', 443)

    cfg.plans = []
    for (const name in env) {
      const value = env[name]
      const match = name.match(/^DEFAULT_PLAN_(.*)$/)
      if (match) {
        cfg.plans.push(JSON.parse(value))
      }
    }
    return cfg
  }

  setupParams (exp) {
    return exp.param('stripeId', this._stripeIdParam)
  }

  setupRoutes (exp) {
    exp.get('/plans', this._getAllPlans)
    exp.get('/plan', this._getAllPlans)
    exp.get('/plan/:stripeId', this._getPlanByStripeId)

    exp.get('/version', this._getVersion)
    exp.get('/live', this.dontLog, this._getLive.bind(this))
    exp.get('/ready', this.dontLog, this._getReady.bind(this))
  }

  getSchema () {
    return {plan: Plan.schema}
  }

  startCustom (callback) {
    debug('Custom startup code')
    debug(`Stripe key = ${this.config.stripeKey}`)
    this.stripe = require('stripe')(this.config.stripeKey)
    if (this.config.stripeHost != null) {
      debug(`Setting Stripe host to ${this.config.stripeHost}:${this.config.stripePort}`)
      this.stripe.setHost(this.config.stripeHost, this.config.stripePort, 'http')
      debug('Finished setting stripe host')
    }

    Plan.stripe = this.stripe

    debug('Getting plans from config')

    const { plans } = this.config

    debug(`Ensuring ${plans.length} plans`)

    async.each(plans, this._ensurePlan.bind(this), callback)
  }

  _ensurePlan (props, callback) {
    debug(`Ensuring plan ${props.stripeId}`)
    Plan.byStripeId(props.stripeId, (err, plan) => {
      if (err) {
        debug(`Error ensuring plan ${props.stripeId}: ${err.message}`)
        callback(err)
      } else if (plan != null) {
        debug(`Found plan ${plan.name} (${props.stripeId})`)
        if (plan.name === 'Free') {
          plan.update({name: 'Free Plan'}, callback)
        } else {
          callback(null)
        }
      } else {
        debug(`Plan ${props.stripeId} not found; creating`)
        Plan.create(props, callback)
      }
    })
  }

  _stripeIdParam (req, res, next, stripeId) {
    return Plan.byStripeId(stripeId, (err, plan) => {
      if (err) {
        return next(err)
      } else if ((plan == null)) {
        return next(new HTTPError('No such plan', 404))
      } else {
        req.plan = plan
        return next()
      }
    })
  }

  _getAllPlans (req, res, next) {
    const plans = []
    const addPlan = function (plan) {
      if (!plan.private) {
        return plans.push(plan)
      }
    }
    return Plan.scan(addPlan, (err) => {
      if (err) {
        return next(err)
      } else {
        return res.json(_.sortBy(plans, 'price'))
      }
    })
  }

  _getPlanByStripeId (req, res, next) {
    res.set('Last-Modified', (new Date(req.plan.updatedAt)).toUTCString())
    return res.json(req.plan)
  }

  _getVersion (req, res, next) {
    return res.json({name: 'planc', version})
  }

  _getLive (req, res, next) {
    return res.json({status: 'OK'})
  }

  _getReady (req, res, next) {
    assert(_.isObject(this))

    if ((this.db == null)) {
      return next(HTTPError('Database not connected', 500))
    }

    return async.parallel([
      callback => {
        // Check that we can contact the database
        return this.db.save('server-ready', 'plan', 1, callback)
      },
      callback => {
        // Check that we can reach Stripe
        // FIXME: this seems like a heavyweight API call to make; maybe
        // there's something slimmer?
        return this.stripe.balance.retrieve(callback)
      }
    ], (err) => {
      if (err != null) {
        debug(`${err.name}: ${err.message} in /ready`)
        return next(err)
      } else {
        return res.json({status: 'OK'})
      }
    })
  }
}

module.exports = PlanServer
