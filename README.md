plan
====

This is the plan microservice for Fuzzy.ai.

Plan
----

Plans are subscription plans for Fuzzy.ai customers. Plan objects have the
following fields:

* id: A unique ID for the plan
* name: A name for the plan, human-readable.
* stripeId: The name we use for this plan on stripe.com. A slug.
* apiLimit: Maximum number of API calls for this period.
* period: Name of the period. We only support "month" now, but maybe someday.
* price: Price of the plan. In pennies, usually.
* currency: Currency for the plan; like "USD".
* createdAt: Creation timestamp.
* updatedAt: Update timestamp.

Environment
-----------

All the same environment variables as Microservice, plus:

* DEFAULT_PLAN_SOMETHING: Adds a plan for "something" to the service. The
  "something" part is unused but must be unique. Value is a JSON string of a
  plan object, except for the ID and timestamps. It will be ensured in the DB.

Endpoints
---------

The server has the following endpoints. All of them require OAuth2 for
authentication, using the appkey authentication.

* GET /plans

  Gets all current plans available in the system. Returns a JSON array with
  one plan object per element.

* GET /plan

  Synonym for /plans.

* GET /plan/:stripeId

  Get the plan with the ID `stripeId`. Returns a single plan object.

* GET /version

  Returns the current version of the server as an object, with a "name" prop
  and a "version" prop. Version is semver.org-compatible.

* GET /live

  Returns `{status: "OK"}` if the server is up.

* GET /ready

  Returns `{status: "OK"}` if the server is up *and* can connect to other
  services (database and Stripe).
