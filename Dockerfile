FROM node:9-alpine

# We need this for our healthcheck

RUN apk add --no-cache curl

WORKDIR /opt/plan

COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/plan/bin/dumb-init", "--"]
CMD ["/usr/local/bin/npm", "start"]
