// api-batch.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const http = require('http')
const childProcess = require('child_process')
const path = require('path')

const vows = require('perjury')
const { assert } = vows
const debug = require('debug')('plan:apibatch')
const _ = require('lodash')

const env = require('./config')

const base = {
  'When we run a Stripe mock': {
    topic () {
      const stripe = http.createServer((req, res) => {
        debug(`Stripe called at: ${req.url}`)
        res.writeHead(200,
          {'Content-Type': 'application/json'})
        res.write('{}')
        return res.end()
      })
      stripe.listen(1516, () => {
        return this.callback(null, stripe)
      })
      return undefined
    },
    'it works' (err, stripe) {
      assert.ifError(err)
    },
    teardown (stripe) {
      const { callback } = this
      stripe.once('close', () => callback(null))
      stripe.close()
      return undefined
    },
    'and we run a plan server': {
      topic () {
        const { callback } = this

        if (process.env.DEBUG != null) {
          env.DEBUG = process.env.DEBUG
        }

        const child = childProcess.fork(path.join(__dirname, '..', 'lib', 'main.js'), [], {env, silent: true})

        child.once('error', err => callback(err))

        child.stderr.on('data', (data) => {
          const str = data.toString('utf8')
          return process.stderr.write(str)
        })

        child.stdout.on('data', (data) => {
          const str = data.toString('utf8')
          if (str.match('Server started')) {
            callback(null, child)
          } else if (env.SILENT !== 'true') {
            return console.log(`SERVER: ${str}`)
          }
        })

        return undefined
      },
      'it works' (err, child) {
        assert.ifError(err)
        assert.isObject(child)
      },
      'teardown' (child) {
        return child.kill()
      }
    }
  }
}

const apiBatch = function (rest) {
  const batch = _.cloneDeep(base)
  _.extend(batch['When we run a Stripe mock']['and we run a plan server'], rest)
  return batch
}

module.exports = apiBatch
