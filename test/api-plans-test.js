// api-plans-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const _ = require('lodash')

const env = require('./config')
const apiBatch = require('./apibatch')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('/plans endpoint')
  .addBatch(apiBatch({
    'and we get the /plans endpoint': {
      topic () {
        const options = {
          url: 'http://localhost:2342/plans',
          json: true,
          headers: {
            'Authorization': `Bearer ${env.APP_KEY_UNIT_TEST}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return this.callback(err)
          } else if (response.statusCode !== 200) {
            return this.callback(new Error(`Unexpected status code ${response.statusCode}: ${body}`))
          } else {
            return this.callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, plans) {
        assert.ifError(err)
        assert.isArray(plans)
        return (() => {
          const result = []
          for (const plan of Array.from(plans)) {
            assert.isObject(plan)
            assert.isString(plan.id)
            assert.isString(plan.name)
            assert.isNumber(plan.apiLimit)
            assert.isString(plan.period)
            assert.isNumber(plan.price)
            assert.isString(plan.currency)
            assert.isString(plan.createdAt)
            result.push(assert.isString(plan.updatedAt))
          }
          return result
        })()
      },
      'it includes the default plans' (err, plans) {
        const expected = []
        for (const name in env) {
          const value = env[name]
          const match = name.match(/^DEFAULT_PLAN_(.*)$/)
          if (match) {
            expected.push(JSON.parse(value))
          }
        }
        assert.ifError(err)
        assert.isArray(plans)
        assert.equal(plans.length, expected.length)
        return Array.from(expected).map((plan) =>
          assert.ok(_.some(plans, result => result.name === plan.name)))
      }
    },
    'and we get the /plan endpoint synonym': {
      topic () {
        const options = {
          url: 'http://localhost:2342/plan',
          json: true,
          headers: {
            'Authorization': `Bearer ${env.APP_KEY_UNIT_TEST}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return this.callback(err)
          } else if (response.statusCode !== 200) {
            return this.callback(new Error(`Unexpected status code ${response.statusCode}: ${body}`))
          } else {
            return this.callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, plans) {
        assert.ifError(err)
        assert.isArray(plans)
      }
    }
  })).export(module)
