// plan-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const http = require('http')

const vows = require('perjury')
const { assert } = vows
const async = require('async')
const {Databank, DatabankObject} = require('databank')

const env = require('./config')

const STRIPE_TEST_KEY = 'test_key'

process.on('uncaughtException', err => console.error(err))

vows
  .describe('Plan data type')
  .addBatch({
    'When we load the plan module': {
      topic () {
        try {
          const Plan = require('../lib/plan')
          this.callback(null, Plan)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, Plan) {
        assert.ifError(err)
        assert.isFunction(Plan)
      },
      'and we set up the database': {
        topic (Plan) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {user: Plan.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              callback(err, null)
            } else {
              DatabankObject.bank = db
              callback(null, db)
            }
          })
          return undefined
        },
        'it works' (err, db) {
          assert.ifError(err)
          assert.isObject(db)
        },
        'teardown' (db) {
          if (db) {
            db.disconnect(this.callback)
          } else {
            this.callback(null)
          }
        },
        'and we setup a mock Stripe server': {
          topic (db, Plan) {
            const { callback } = this
            Plan.stripe = require('stripe')(STRIPE_TEST_KEY)
            Plan.stripe.setHost('localhost', 1516, 'http')
            const stripe = http.createServer((req, res) => {
              res.writeHead(200,
                {'Content-Type': 'application/json'})
              res.write('{}')
              return res.end()
            })
            stripe.listen(1516, () => callback(null, stripe))
            return undefined
          },
          'it works' (err, stripe) {
            assert.ifError(err)
          },
          teardown (stripe) {
            const { callback } = this
            stripe.once('close', () => callback(null))
            stripe.close()
            return undefined
          },
          'and we create a new Plan': {
            topic (stripe, db, Plan) {
              const { callback } = this
              return async.parallel([
                callback =>
                  stripe.once('request', (req, res) => callback(null)),
                function (callback) {
                  const props = {
                    name: 'Unit test',
                    stripeId: 'unit_test',
                    apiLimit: 250000,
                    period: 'calendar month',
                    price: 4900,
                    currency: 'USD'
                  }
                  return Plan.create(props, (err, plan) => {
                    if (err) {
                      callback(err)
                    } else {
                      callback(null, plan)
                    }
                  })
                }
              ], (err, results) => {
                if (err) {
                  callback(err)
                } else {
                  callback(null, results[1])
                }
              })
            },
            'it works' (err, plan) {
              assert.ifError(err)
              assert.isObject(plan)
              assert.isString(plan.id)
              assert.isString(plan.name)
              assert.isString(plan.stripeId)
              assert.isNumber(plan.apiLimit)
              assert.isString(plan.period)
              assert.isNumber(plan.price)
              assert.isString(plan.currency)
              assert.isString(plan.createdAt)
              assert.inDelta(Date.parse(plan.createdAt), Date.now(), 5000)
              assert.isString(plan.updatedAt)
              assert.inDelta(Date.parse(plan.updatedAt), Date.now(), 5000)
            },
            'and we wait 2 seconds': {
              topic (plan, stripe) {
                const { callback } = this
                const wait = () => callback(null)
                setTimeout(wait, 2000)
                return undefined
              },
              'it works' (err) {
                assert.ifError(err)
              },
              'and we update the plan': {
                topic (plan) {
                  plan.update({apiLimit: 500000}, this.callback)
                  return undefined
                },
                'it works' (err, plan) {
                  assert.ifError(err)
                  assert.isObject(plan)
                  assert.isString(plan.id)
                  assert.isString(plan.stripeId)
                  assert.isNumber(plan.apiLimit)
                  assert.equal(plan.apiLimit, 500000)
                  assert.isString(plan.updatedAt)
                  assert.notEqual(plan.createdAt, plan.updatedAt)
                  assert.inDelta(Date.parse(plan.updatedAt), Date.now(), 5000)
                }
              }
            }
          }
        }
      }
    }}).export(module)
