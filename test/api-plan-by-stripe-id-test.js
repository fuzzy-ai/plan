// api-plans-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const env = require('./config')
const apiBatch = require('./apibatch')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('/plan/:stripeId endpoint')
  .addBatch(apiBatch({
    'and we get the /plan/:stripeId endpoint': {
      topic () {
        const options = {
          url: 'http://localhost:2342/plan/unit_test_1',
          json: true,
          headers: {
            'Authorization': `Bearer ${env.APP_KEY_UNIT_TEST}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return this.callback(err)
          } else if (response.statusCode !== 200) {
            const code = response.statusCode
            const msg = (body != null ? body.message : undefined) ? (body != null ? body.message : undefined) : body
            return this.callback(new Error(`Unexpected status code ${code}: ${msg}`))
          } else {
            return this.callback(null, response, body)
          }
        })
        return undefined
      },
      'it works' (err, response, plan) {
        assert.ifError(err)
        assert.isObject(plan)
        assert.isString(plan.id)
        assert.isString(plan.name)
        assert.isNumber(plan.apiLimit)
        assert.isString(plan.period)
        assert.isNumber(plan.price)
        assert.isString(plan.currency)
        assert.isString(plan.createdAt)
        assert.isString(plan.updatedAt)
      },
      'Last-Modified date is correct' (err, response, plan) {
        assert.ifError(err)
        assert.isString(response.headers['last-modified'])
        // Last-Modified doesn't include seconds, so truncate to compare
        const lmDate = new Date(response.headers['last-modified'])
        const uaDate = new Date(plan != null ? plan.updatedAt : undefined)
        const roundMS = function (dt) {
          const tm = dt.getTime()
          return Math.floor(tm / 1000.0) * 1000
        }
        assert.equal(roundMS(lmDate), roundMS(uaDate))
      }
    }
  })).export(module)
