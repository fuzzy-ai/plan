// config.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const env = {
  PORT: '2342',
  DRIVER: 'memory',
  PARAMS: '{}',
  APP_KEY_UNIT_TEST: 'verdipuckrinkbanwilt',
  LOG_FILE: '/dev/null',
  SILENT: 'true',
  DEFAULT_PLAN_UNIT_TEST_1: JSON.stringify({
    name: 'Unit Test 1',
    stripeId: 'unit_test_1',
    apiLimit: 25000,
    period: 'month',
    price: 2500,
    currency: 'USD'
  }),
  STRIPE_KEY: 'test_key',
  STRIPE_HOST: 'localhost',
  STRIPE_PORT: '1516'
}

module.exports = env
