// planserver-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const http = require('http')

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const debug = require('debug')

process.on('uncaughtException', err => console.error(err))

const env = require('./config')

vows
  .describe('plan server')
  .addBatch({
    'When we run a Stripe mock': {
      topic () {
        const stripe = http.createServer((req, res) => {
          debug(`Stripe called at: ${req.url}`)
          res.writeHead(200,
            {'Content-Type': 'application/json'})
          res.write('{}')
          return res.end()
        })
        stripe.listen(1516, () => {
          return this.callback(null, stripe)
        })
        return undefined
      },
      'it works' (err, stripe) {
        assert.ifError(err)
      },
      teardown (stripe) {
        const { callback } = this
        stripe.once('close', () => callback(null))
        stripe.close()
        return undefined
      },
      'and we load the module': {
        topic () {
          const { callback } = this
          try {
            const PlanServer = require('../lib/planserver')
            callback(null, PlanServer)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, PlanServer) {
          assert.ifError(err)
        },
        'it is a class' (err, PlanServer) {
          assert.ifError(err)
          assert.isFunction(PlanServer)
        },
        'and we instantiate a PlanServer': {
          topic (PlanServer) {
            const { callback } = this
            try {
              const server = new PlanServer(env)
              callback(null, server)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'it is an object' (err, server) {
            assert.ifError(err)
            assert.isObject(server)
          },
          'it has a start() method' (err, server) {
            assert.ifError(err)
            assert.isObject(server)
            assert.isFunction(server.start)
          },
          'it has a stop() method' (err, server) {
            assert.ifError(err)
            assert.isObject(server)
            assert.isFunction(server.stop)
          },
          'and we start the server': {
            topic (server) {
              const { callback } = this
              server.start((err) => {
                if (err) {
                  callback(err)
                } else {
                  callback(null)
                }
              })
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we request the version': {
              topic () {
                const { callback } = this
                const url = 'http://localhost:2342/version'
                request.get(url, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 200) {
                    callback(new Error(`Bad status code ${response.statusCode}`))
                  } else {
                    body = JSON.parse(body)
                    callback(null, body)
                  }
                })
                return undefined
              },
              'it works' (err, version) {
                assert.ifError(err)
              },
              'it looks correct' (err, version) {
                assert.ifError(err)
                assert.include(version, 'version')
                assert.include(version, 'name')
              },
              'and we stop the server': {
                topic (version, server) {
                  const { callback } = this
                  server.stop((err) => {
                    if (err) {
                      callback(err)
                    } else {
                      callback(null)
                    }
                  })
                  return undefined
                },
                'it works' (err) {
                  assert.ifError(err)
                },
                'and we request the version': {
                  topic () {
                    const { callback } = this
                    const url = 'http://localhost:2342/version'
                    request.get(url, (err, response, body) => {
                      if (err) {
                        callback(null)
                      } else {
                        callback(new Error('Unexpected success after server stop'))
                      }
                    })
                    return undefined
                  },
                  'it fails correctly' (err) {
                    assert.ifError(err)
                  }
                }
              }
            }
          }
        }
      }
    }
  })
  .export(module)
